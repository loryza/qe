FROM quay.io/loryza/nersc-mpich:latest

RUN mkdir -p /mnt/xetta/host

RUN mkdir -p /mnt/xetta/src

RUN cd /mnt/xetta/src && \
    wget http://www.fftw.org/fftw-3.3.5.tar.gz && \
    tar xfz fftw-3.3.5.tar.gz && \
    cd fftw-3.3.5 && \
    ./configure MPICC=/mnt/xetta/bin/mpicc --enable-mpi --prefix=/mnt/xetta && \
    make install

RUN cd /mnt/xetta/src && \ 
    wget http://www.qe-forge.org/gf/download/frsrelease/224/1044/qe-6.0.tar.gz && \
    tar xfz qe-6.0.tar.gz && \
    cd qe-6.0 && \
    LIBDIRS=/mnt/xetta/lib MPIF90=/mnt/xetta/bin/mpif90 ./configure --enable-openmp --enable-parallel --enable-shared  \
    --with-hdf5 --prefix=/mnt/xetta -with-internal-blas -with-internal-lapack  && \
    make all && \
    make install
